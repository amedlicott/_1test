package com.digitalml.rest.resources.codegentest.application;

import com.digitalml.rest.resources.codegentest.resource._1testResource;
import com.digitalml.rest.resources.codegentest.resource.ResourceLoggingFilter;					
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import com.codahale.metrics.health.HealthCheck;
import io.dropwizard.Configuration;

import java.util.EnumSet;
import javax.servlet.DispatcherType;

import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.util.ContextInitializer;
import ch.qos.logback.core.joran.spi.JoranException;


public class _1testApplication extends Application<_1testConfiguration> {

	public static void main(final String[] args) throws Exception {
		new _1testApplication().run(args);
	}
	
	@Override
	public void initialize(final Bootstrap<_1testConfiguration> bootstrap) {
		// TODO: application initialization
	}

	@Override
	public void run(final _1testConfiguration configuration, final Environment environment) {

		//separate logging setup to enable use of external logback xml
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
		context.reset();
		ContextInitializer initializer = new ContextInitializer(context);
		try {
			initializer.autoConfig();
		} catch (JoranException e) {
		}

		final _1testResource resource = new _1testResource();
		final _1testHealthCheck healthCheck = new _1testHealthCheck();
		
		environment.servlets().addFilter("ResourceLoggingFilter", new ResourceLoggingFilter()).addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
		
		environment.healthChecks().register("template", healthCheck);
		environment.jersey().register(resource);

	}
}

final class _1testConfiguration extends Configuration {
}

final class _1testHealthCheck extends HealthCheck {

	public _1testHealthCheck() {
	}

	@Override
	protected Result check() throws Exception {
		return Result.healthy();
	}
}